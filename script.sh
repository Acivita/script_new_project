if test -d $1;\
then echo "$1 : directory already exist";\
else
mkdir $1
cd $1
git init
git remote add origin https://Acivita@bitbucket.org/Acivita/$1.git
git submodule add https://Acivita@bitbucket.org/Acivita/libft.git
mkdir srcs
mkdir includes

echo "#include \"$1.h\"

int main(void)
{
	return (0);
}" > ./srcs/main.c

echo "#ifndef \c" > ./includes/$1.h
echo "_$1_H" | tr [a-z] [A-Z] >> ./includes/$1.h
echo "# define \c" >> ./includes/$1.h
echo "_$1_H\n" | tr [a-z] [A-Z] >> ./includes/$1.h
echo "#include \"libft.h\"\n" >> ./includes/$1.h
echo "#endif" >> ./includes/$1.h

echo "NAME = $1\n" > Makefile 
echo "SRC = main.c\n" >> Makefile

echo "FLAGS = -g -Wall -Werror -Wextra" >> Makefile
echo "HEADER = -Iincludes/ -Ilibft/includes" >> Makefile
echo "OBJ2 = \$(addprefix obj/, \$(notdir \$(OBJ)))" >> Makefile
echo "OBJ = \$(SRC:.c=.o)" >> Makefile
echo "LIB = libft/libft.a\n" >> Makefile
echo ".SILENT :\n" >> Makefile
echo "all: update_submodules mkd \$(NAME)\n" >> Makefile
echo "update_submodules:
	if test -d libft;\\" >> Makefile
echo "\tthen git submodule update --init -q;\\" >> Makefile
echo "\telse git clone https://Acivita@bitbucket.org/Acivita/libft.git;\\" >> Makefile
echo "\tfi
	@make -f Makefile -C libft\n" >> Makefile
echo "update_lib:
	git submodule update --init -q
	make -f Makefile -C libft\n" >> Makefile
echo "clean_lib:
	git submodule update --init -q
	make -f Makefile -C libft clean\n" >> Makefile
echo "fclean_lib:
	git submodule update --init -q
	make -f Makefile -C libft fclean\n" >> Makefile
echo "obj/%.o : srcs/%.c
	@gcc -g \$(FLAGS) \$(HEADER) -c -o \$@ \$^" >> Makefile
echo "	printf \"\\33[2K\\\r\"" >> Makefile
echo "	printf \"\\\033[32m[✔]\\\033[36m \$@\"" >> Makefile
echo "\nmkd:
	mkdir -p obj\n" >> Makefile
echo "\$(NAME): \$(OBJ2)
	gcc -g \$(FLAGS) \$(OBJ2) \$(HEADER) \$(LIB) -o \$(NAME)" >> Makefile
echo "	printf \"\\\033[32m\\\n----------------------------------\\\\033[36m\\\n\"" >> Makefile
echo "	printf \"\\\033[32m[✔]\\\033[36m \$@\"" >> Makefile
echo "	printf \"\\\n\\\033[32m----------------------------------\\\033[36m" >> Makefile
echo "	printf \"\\\033[0m\\\n\"\n" >> Makefile
echo "clean: clean_lib
		rm -rf obj\n" >> Makefile
echo "fclean: fclean_lib clean
		rm -rf \$(NAME)\n" >> Makefile
echo "re: fclean all\n" >> Makefile
echo ".PHONY: re fclean clean all" >> Makefile
fi
